import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        BubbleSort bs=new BubbleSort();
        int arr[] = { 64, 34, 25, 12, 22, 11, 90 };

        bs.bubbleSort(arr);
        bs.printArray(arr);
    }
}
